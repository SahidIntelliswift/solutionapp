//
//  CitrusPayment.h
//  CitrusPay
//
//  Created by Sahid on 31/05/16.
//  Copyright © 2016 intelliswift. All rights reserved.
//

#import <Foundation/Foundation.h>
#import <UIKit/UIKit.h>


@protocol CitrusPayDelegate <NSObject>

-(void) transactionSuceesWithTransactionId :(NSString *)transactionId andWithStatus:(NSString *)Status;

@end

@interface CitrusPayment : NSObject{

}

@property (nonatomic,weak) id<CitrusPayDelegate> citrusDelegate;
+ (CitrusPayment *)sharedInstance;
-(void)initializeLayers;
-(void)SubmitPaymentWithCardType:(NSInteger )cardType andWithDetails:(NSMutableDictionary *)details withViewControllet:(UIViewController *) viewController;
-(NSArray *)GetCitrusBanks;
-(void)updatePaymentMethod:(NSInteger )paymentType;
-(void)saveCards;
-(void)getSaveCards;
-(void)deleteSaveCards;
-(NSString*)processString :(NSString*)yourString;
@end
