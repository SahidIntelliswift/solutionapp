//
//  CitrusPay.m
//  CitrusPay
//
//  Created by Sahid on 31/05/16.
//  Copyright © 2016 intelliswift. All rights reserved.
//

#import "CitrusPayment.h"
#import "CTSProfileLayer.h"
#import "CTSPaymentLayer.h"
#import "MerchantConstants.h"
#import "TestParams.h"
#import "UIUtility.h"
#import "CitrusSdk.h"

@interface CitrusPayment(){
    
    CTSProfileLayer *profileLayer;
    CTSAuthLayer *authLayer;
    CTSPaymentLayer *paymentLayer;
    CTSElectronicCardUpdate *card;
    CTSPaymentDetailUpdate *paymentInfo;
    CTSContactUpdate* contactInfo;
    CTSUserAddress* addressInfo;
    CTSNetBankingUpdate* netBank ;
    NSInteger paymentMethod;
    
    
}

@end
@implementation CitrusPayment

static CitrusPayment *citrusInstance = nil;

+ (CitrusPayment *)sharedInstance
{
    static dispatch_once_t onceToken;
    dispatch_once(&onceToken, ^{
        if(!citrusInstance)
            citrusInstance = [[CitrusPayment alloc] init];
        
    });
    return citrusInstance;
}
-(void)initializeLayers{
    
    CTSKeyStore *keyStore = [[CTSKeyStore alloc] init];
    keyStore.signinId = SignInId;
    keyStore.signinSecret = SignInSecretKey;
    keyStore.signUpId = SubscriptionId;
    keyStore.signUpSecret = SubscriptionSecretKey;
    keyStore.vanity = VanityUrl;
    // [CitrusPaymentSDK initializeWithKeyStore:keyStore environment:CTSEnvSandbox];
    
    [CitrusPaymentSDK initializeWithKeyStore:keyStore environment:CTSEnvProduction];
    authLayer = [CTSAuthLayer fetchSharedAuthLayer];
    
    //    contactInfo = [[CTSContactUpdate alloc] init];
    //    contactInfo.firstName = TEST_FIRST_NAME;
    //    contactInfo.lastName = TEST_LAST_NAME;
    //    contactInfo.email = TEST_EMAIL;
    //    contactInfo.mobile = TEST_MOBILE;
    //
    //    addressInfo = [[CTSUserAddress alloc] init];
    //    addressInfo.city = TEST_CITY;
    //    addressInfo.country = TEST_COUNTRY;
    //    addressInfo.state = TEST_STATE;
    //    addressInfo.street1 = TEST_STREET1;
    //    addressInfo.street2 = TEST_STREET2;
    //    addressInfo.zip = TEST_ZIP;
    //
    //    customParams = @{
    //                     @"USERDATA2":@"MOB_RC|9988776655",
    //                     @"USERDATA10":@"test",
    //                     @"USERDATA4":@"MOB_RC|test@gmail.com",
    //                     @"USERDATA3":@"MOB_RC|4111XXXXXXXX1111",
    //                     };
    
    profileLayer=[CitrusPaymentSDK fetchSharedProfileLayer];
    paymentLayer= [CitrusPaymentSDK fetchSharedPaymentLayer];
    
}

-(void)SubmitPaymentWithCardType:(NSInteger)cardType andWithDetails:(NSMutableDictionary *)details withViewControllet:(UIViewController *)viewController
{
     paymentInfo= [[CTSPaymentDetailUpdate alloc] init];
     switch (paymentMethod) {
            
            //card type 0 for CreditCard
        case 0:
            card = [[CTSElectronicCardUpdate alloc] initCreditCard];
            card.number = [details valueForKey:@"CardNo"];
            card.expiryDate = [details valueForKey:@"ExpiryDate"];
            card.scheme = [CTSUtility fetchCardSchemeForCardNumber:card.number];
            card.ownerName = [details valueForKey:@"CardName"];
            card.cvv = [details valueForKey:@"Cvv"];
          //  paymentInfo= [[CTSPaymentDetailUpdate alloc] init];
            [paymentInfo addCard:card];
             
            
            break;
            
            //card type 1 for DebitCard
        case 1:
            
            card = [[CTSElectronicCardUpdate alloc] initDebitCard];
            card.number = [details valueForKey:@"CardNo"];
            card.expiryDate = [details valueForKey:@"ExpiryDate"];
            card.scheme = [CTSUtility fetchCardSchemeForCardNumber:card.number];
            card.ownerName = [details valueForKey:@"CardName"];
            card.cvv = [details valueForKey:@"Cvv"];
           // paymentInfo= [[CTSPaymentDetailUpdate alloc] init];
            [paymentInfo addCard:card];
            
            break;
            
            //card type 2 for netBanking
        case 2:
            
            netBank = [[CTSNetBankingUpdate alloc] init];
            netBank.code = [details valueForKey:@"BankCode"]; //you can obtain this from pgSetting service
            netBank.name = [details valueForKey:@"BankName"];
            [paymentInfo addNetBanking:netBank];
            
            break;
            
        default:
            
            break;
    }
    
    NSString *billUrl1=[NSString stringWithFormat:@"%@?cust_id=%@&addr_id=%@&amount=%@",BillUrl,[details valueForKey:@"CustID"],[details valueForKey:@"address"],[details valueForKey:@"amount"]];
    [CTSUtility requestBillAmount:@"" billURL:billUrl1 callback:^(CTSBill *bill, NSError *error) {
        
        if(error == nil){
            
            [paymentLayer requestChargePayment:paymentInfo withContact:contactInfo withAddress:addressInfo bill:bill customParams:@{@"Test":@"cust"} returnViewController:viewController withCompletionHandler:^(CTSCitrusCashRes *citrusCashResponse, NSError *error) {
                if(error){
                    [UIUtility toastMessageOnScreen:error.localizedDescription];
                }
                else {
                    if ([[citrusCashResponse.responseDict valueForKey:@"TxStatus"] isEqualToString:@"SUCCESS"]) {
                        
                        
                        NSString *orderNo=[citrusCashResponse.responseDict valueForKey:@"TxId"];
                        [self.citrusDelegate transactionSuceesWithTransactionId:orderNo andWithStatus:[citrusCashResponse.responseDict valueForKey:@"TxStatus"]];
                        
                    }else{
                        [UIUtility toastMessageOnScreen:[NSString stringWithFormat:@"Payment Status %@",[citrusCashResponse.responseDict valueForKey:@"TxStatus"] ]];
                    }
                    
                }
            }];
        }
        else {
            [UIUtility toastMessageOnScreen:[NSString stringWithFormat:@"Bill Fetch Error : %@",error.localizedDescription]];
        }
    }];
    
    
}
-(void)updatePaymentMethod:(NSInteger )paymentType{
    paymentMethod = paymentType;

}

//-(NSArray *)GetCitrusBanks{
//    [paymentLayer requestMerchantPgSettings:VanityUrl withCompletionHandler:^(CTSPgSettings *pgSettings, NSError *error) {
//        if(error){
//            //handle error
//            return <#expression#>
//        }
//        else {
//            //           banksArray=pgSettings.netBanking;
//            
//            NSSortDescriptor *sort = [NSSortDescriptor sortDescriptorWithKey:@"bankName" ascending:YES];
//            return [pgSettings.netBanking sortedArrayUsingDescriptors:@[sort]];
//
//        }
//}
-(void)saveCards{
    // Save the card
    [profileLayer updatePaymentInformation:paymentInfo withCompletionHandler:^(NSError *error) {
        if(error == nil){
            // Your code to handle success.
        }
        else {
            // Your code to handle error.
        }
    }];
}

-(void)getSaveCards{
    [profileLayer requestPaymentInformationWithCompletionHandler:^(CTSProfilePaymentRes *paymentInfos, NSError *error) {
        if (error == nil) {
            // Your code to handle success.
            if([paymentInfos.paymentOptions count]){
                //process the save cards here
            }
            else{
                // no saved cards
            }
        } else {
            // Your code to handle error.
        }
    }];

}
-(void)deleteSaveCards{
    [profileLayer requestDeleteCardWithToken:card.token withCompletionHandler:^(NSError *error) {
        if(error == nil){
            // Your card is successfully deleted.
        }
        else {
            // Your code to handle error.
        }
    }];
}
-(NSString*)processString :(NSString*)yourString
{
    if(yourString == nil){
        return @"";
    }
    int stringLength = (int)[yourString length];
    // the string you want to process
    int len = 4;  // the length
    NSMutableString *str = [NSMutableString string];
    int i = 0;
    for (; i < stringLength; i+=len) {
        NSRange range = NSMakeRange(i, len);
        [str appendString:[yourString substringWithRange:range]];
        if(i!=stringLength -4){
            [str appendString:@" "]; //If required stringshould be in format XXXX-XXXX-XXXX-XXX then just replace [str appendString:@"-"]
        }
    }
    if (i < [str length]-1) {  // add remain part
        [str appendString:[yourString substringFromIndex:i]];
    }
    // str now is what your want
    
    return str;
}
@end
