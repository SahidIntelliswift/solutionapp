//
//  MerchantConstants.h
//  CTS iOS Sdk
//
//  Created by Sahid Saiyed on 13/08/14.

//


#ifndef CTS_iOS_Sdk_MerchantConstants_h
#define CTS_iOS_Sdk_MerchantConstants_h

// for sandbox
//#define VanityUrl @"specimenapp"
//#define SignInId @"os1s0z9tft-signin"
//#define SignInSecretKey @"01f52b3e4ecd5d56d7cedcc2e5512821"
//#define SubscriptionId @"os1s0z9tft-signup"
//#define SubscriptionSecretKey @"dde8399389e03529ff3f76c804b8e62c"


//for production

#define VanityUrl @"specimen"
#define SignInId @"66hf5fktkl-signin"
#define SignInSecretKey @"2284cab3f12beb4e4e8b4aba64b946f6"
#define SubscriptionId @"66hf5fktkl-signup"
#define SubscriptionSecretKey @"a92827bf1bfd35ce6a73f71c2464c271"




// URLs
//#define LoadWalletReturnUrl @"https://salty-plateau-1529.herokuapp.com/redirectURL.sandbox.php"

//#define BillUrl @"https://salty-plateau-1529.herokuapp.com/billGenerator.sandbox.php"


//bill url that provide by server side team
//#define BillUrl @"http://specimen.intelliswift.in/citruspayinfo/citruspayinfo/index/"
#define BillUrl @"http://specimenqa.intelliswift.in/citruspayinfo/citruspayinfo/index"

#endif


//{"access_key":"HYDLZNEC18M0T1TXA2JR","JS-signin-id":"66hf5fktkl-JS-signin","JS-signin-secret":"11a2bc40978e6f5206ca13dc4dce5741","vanity":"specimen"}


//{"access_key":"HYDLZNEC18M0T1TXA2JR","signup-id":"66hf5fktkl-signup","signup-secret":"a92827bf1bfd35ce6a73f71c2464c271","signin-id":"66hf5fktkl-signin","signin-secret":"2284cab3f12beb4e4e8b4aba64b946f6","vanity":"specimen"}
